def run(line):
    print("Run file {file}. Send message:\n {line}.".format(
        file=__file__,
        line=line
    ))
