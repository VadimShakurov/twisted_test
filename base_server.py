from twisted.internet.protocol import Factory, Protocol
# from twisted.protocols.basic import LineReceiver


class BaseServer(Protocol):
    def __init__(self, clients, run_module=None):
        self.clients = clients
        self.run_module = run_module
    
    def connectionMade(self):
        pass

    def dataReceived(self, data):
        print('*'*88)
        print(data)
        if self.run_module:
            self.run_module.run(data)

        elif b'HTTP/1.1' in data:
            message = data.split(b'\r\n\r\n')[-1]
            self.transport.write(b'HTTP/1.1 200 OK')
            self.clients['client'].transport.write(message)
            return self.transport.loseConnection()

        else:
            self.clients['client'] = self


class ServerFactory(Factory):
    def __init__(self, run_module=None):
        self.run_module = run_module
        self.clients = {}
    
    def buildProtocol(self, addr):
        return BaseServer(self.clients, self.run_module)
